create database colaborador;
\c colaborador

-- CREATE TABLE public.setor;

CREATE TABLE IF NOT EXISTS public.setor (
	id int8 NOT NULL,
	descricao varchar(200) NOT NULL,
	CONSTRAINT pk_SETOR PRIMARY KEY (id)
);

-- CREATE TABLE public.colaborador;

CREATE TABLE IF NOT EXISTS public.colaborador (
	id int8 NOT NULL,
	cpf varchar(16) NOT NULL,
	dtnascimento timestamp NULL,
	email varchar(200) NULL,
	nome varchar(200) NOT NULL,
	telefone varchar(11) NULL,
	id_setor int8 NULL,
	CONSTRAINT pk_colaborador PRIMARY KEY (id),
	CONSTRAINT fk_setor FOREIGN KEY (id_setor) REFERENCES setor(id)
);

DROP SEQUENCE IF EXISTS public.seq_colaborador;

-- CREATE SEQUENCE public.seq_colaborador;

CREATE SEQUENCE public.seq_colaborador
	INCREMENT BY 10
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 1;

-- CADASTRAR SETOR

INSERT INTO public.setor(id,descricao)VALUES(2,'Setor RH');
INSERT INTO public.setor(id,descricao)VALUES(3,'Setor Auditoria');
INSERT INTO public.setor(id,descricao)VALUES(4,'Setor Tecnologia');

-- CADASTRAR COLABORADOR

INSERT INTO public.colaborador(id,cpf,dtnascimento,email,nome,telefone,id_setor)
VALUES
(nextval('seq_colaborador'), '1111111111111111','1980/12/01','mmmmmmm','João','1234',2);

INSERT INTO public.colaborador(id,cpf,dtnascimento,email,nome,telefone,id_setor)
VALUES
(nextval('seq_colaborador'), '2222222222222222','1980/10/01','mmmmmmm','Maria','1234',2);

INSERT INTO public.colaborador(id,cpf,dtnascimento,email,nome,telefone,id_setor)
VALUES
(nextval('seq_colaborador'), '3333333333333333','1960/11/01','mmmmmmm','Sergio','1234',3);

INSERT INTO public.colaborador(id,cpf,dtnascimento,email,nome,telefone,id_setor)
VALUES
(nextval('seq_colaborador'), '4444444444444444','1980/05/01','mmmmmmm','Tania','1234',2);

INSERT INTO public.colaborador(id,cpf,dtnascimento,email,nome,telefone,id_setor)
VALUES
(nextval('seq_colaborador'), '5555555555555555','1980/11/01','mmmmmmm','Mariana','1234',2);

INSERT INTO public.colaborador(id,cpf,dtnascimento,email,nome,telefone,id_setor)
VALUES
(nextval('seq_colaborador'), '6666666666666666','2005/11/01','mmmmmmm','Bruna','1234',2);
