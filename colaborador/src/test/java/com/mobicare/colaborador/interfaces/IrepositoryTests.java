package com.mobicare.colaborador.interfaces;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.text.ParseException;

public interface IrepositoryTests {
	public void saveOrUpdate() throws ParseException, JsonProcessingException;
}
