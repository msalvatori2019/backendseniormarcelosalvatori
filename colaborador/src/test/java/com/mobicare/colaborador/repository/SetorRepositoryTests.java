package com.mobicare.colaborador.repository;


import com.mobicare.colaborador.interfaces.IrepositoryTests;
import com.mobicare.colaborador.model.Setor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SetorRepositoryTests implements IrepositoryTests {

    @Autowired
    SetorRepository setorRepository;



    Setor setor;


    public void setUp() {}


    @Transactional
    @Test
    @Override
    public void saveOrUpdate()  throws ParseException {

        /**Inclusao Setor
         * @author Marcelo Salvatori
         * @param descricao - descricao do setor
         * @return Setor - Valor do resultado da inclusão
         */
        this.setor = new Setor();
        this.setor.setDescricao("Setor A");
        this.setor.setId(1L);

        this.setor = setorRepository.save(this.setor);
        assertThat(this.setor.getId()).isNotNull();

        /**Alteração Setor
         * @author Marcelo Salvatori
         * @param descricao - descricao do setor
         * @return Setor  - Valor do resultado da alteração
         */

        Optional<Setor> setoropt = setorRepository.findById(this.setor.getId());
        Setor setor1 = new Setor();
        setor1 = setoropt.get();
        setor1.setDescricao("Setor B");
        Setor setor2 = new Setor();
        setor2 = setor1;
        this.setor = setorRepository.save(setor1);
        assertThat(setor2).isEqualTo(this.setor);

        /**Exclusão da Setor e validação
         * @author Marcelo Salvatori
         * @return true
         */
        setorRepository.delete(this.setor);
        Optional<Setor> setornull = setorRepository.findById(setor2.getId());
        assertThat(setornull.isEmpty());

    }

}
