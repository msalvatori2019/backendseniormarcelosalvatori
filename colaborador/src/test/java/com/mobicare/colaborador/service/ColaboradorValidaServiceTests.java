package com.mobicare.colaborador.service;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.mobicare.colaborador.interfaces.IrepositoryTests;
import com.mobicare.colaborador.model.Colaborador;
import com.mobicare.colaborador.model.Setor;
import com.mobicare.colaborador.repository.ColaboradorRepository;
import com.mobicare.colaborador.repository.SetorRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ColaboradorValidaServiceTests implements IrepositoryTests {

    @Autowired
    SetorRepository setorRepository;

    @Autowired
    ColaboradorRepository colaboradorRepository;

    @Autowired
    ColaboradorService colaboradorService;

    Setor setor;

    @Transactional
    @Test
    @Override
    public void saveOrUpdate() throws ParseException, JsonProcessingException {
    /**Inclusao Setor
     * @author Marcelo Salvatori
     * @param descricao - descricao do setor
     * @return Setor - Valor do resultado da inclusão
     */
        this.setor = new Setor();
        this.setor.setDescricao("Setor A");
        this.setor.setId(1L);

        this.setor = setorRepository.save(this.setor);
        assertThat(this.setor.getId()).isNotNull();

        /**Inclusao Colaborador
         * @author Marcelo Salvatori
         * não inclui Menor de 18 anos e contem na blacklist
         */

        Colaborador colaborador =  new Colaborador();
        colaborador = new Colaborador();
        colaborador.setCpf("77386499094");
        colaborador.setNome("Menor de 18 anos");
        colaborador.setTelefone("99999999");
        colaborador.setEmail("m.mmm@mmmm.mmm.nn");
        String data = "10/10/2005";
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        colaborador.setDtnascimento(sdf.parse(data));
        colaborador.setSetor(setor);

        assertThat(colaboradorService.validaIdade(colaborador.getDtnascimento())).isEqualTo(false);

        assertThat(colaboradorService.validaBlackList(colaborador.getCpf())).isEqualTo(true);

        Colaborador colaborador2 =  new Colaborador();
        assertThat(colaboradorService.save(colaborador)).isEqualTo(colaborador2);


        /**Inclusao Colaborador
         * @author Marcelo Salvatori
         * não inclui Maior de 18 anos e contem na blacklist
         */

        Colaborador colaborador3 =  new Colaborador();
        colaborador3 = new Colaborador();
        colaborador3.setCpf("77386499094");
        colaborador3.setNome("Maior de 18 anos");
        colaborador3.setTelefone("99999999");
        colaborador3.setEmail("m.mmm@mmmm.mmm.nn");
        String data2 = "10/10/1980";
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy");
        colaborador3.setDtnascimento(sdf2.parse(data2));
        colaborador3.setSetor(setor);

        assertThat(colaboradorService.validaIdade(colaborador3.getDtnascimento())).isEqualTo(true);

        assertThat(colaboradorService.validaBlackList(colaborador3.getCpf())).isEqualTo(true);

        assertThat(colaboradorService.save(colaborador3)).isNotNull();


        /**Inclusao Colaborador
         * @author Marcelo Salvatori
         * inclui Maior de 18 anos e não contem na blacklist
         */

        Colaborador colaborador4 =  new Colaborador();
        colaborador4 = new Colaborador();
        colaborador4.setCpf("111111111111");
        colaborador4.setNome("Maior de 18 anos");
        colaborador4.setTelefone("99999999");
        colaborador4.setEmail("m.mmm@mmmm.mmm.nn");
        String data3 = "10/10/1980";
        SimpleDateFormat sdf3 = new SimpleDateFormat("dd/MM/yyyy");
        colaborador4.setDtnascimento(sdf3.parse(data2));
        colaborador4.setSetor(setor);

        assertThat(colaboradorService.validaIdade(colaborador4.getDtnascimento())).isEqualTo(true);

        assertThat(colaboradorService.validaBlackList(colaborador4.getCpf())).isEqualTo(false);

        assertThat(colaboradorService.save(colaborador4)).isNotNull();


        /**Inclusao Colaborador
         * @author Marcelo Salvatori
         * inclui Maior de 65 anos e não contem na blacklist
         */

        Colaborador colaborador5 =  new Colaborador();
        colaborador5 = new Colaborador();
        colaborador5.setCpf("111111111111");
        colaborador5.setNome("Maior de 65 anos");
        colaborador5.setTelefone("99999999");
        colaborador5.setEmail("m.mmm@mmmm.mmm.nn");
        String data4 = "10/10/1950";
        SimpleDateFormat sdf4 = new SimpleDateFormat("dd/MM/yyyy");
        colaborador5.setDtnascimento(sdf4.parse(data4));
        colaborador5.setSetor(setor);

        assertThat(colaboradorService.validaIdade(colaborador5.getDtnascimento())).isEqualTo(true);

        assertThat(colaboradorService.validaBlackList(colaborador5.getCpf())).isEqualTo(false);

        assertThat(colaboradorService.save(colaborador5)).isNotNull();

        /**Inclusao Colaborador
         * @author Marcelo Salvatori
         * não inclui Maior de 65 anos e não contem na blacklist
         */

        Colaborador colaborador6 =  new Colaborador();
        colaborador6 = new Colaborador();
        colaborador6.setCpf("111111111111");
        colaborador6.setNome("Maior de 65 anos");
        colaborador6.setTelefone("99999999");
        colaborador6.setEmail("m.mmm@mmmm.mmm.nn");
        String data5 = "10/10/1950";
        SimpleDateFormat sdf5 = new SimpleDateFormat("dd/MM/yyyy");
        colaborador6.setDtnascimento(sdf5.parse(data5));
        colaborador6.setSetor(setor);

        assertThat(colaboradorService.validaIdade(colaborador6.getDtnascimento())).isEqualTo(true);

        assertThat(colaboradorService.validaBlackList(colaborador6.getCpf())).isEqualTo(false);

        Colaborador colaborador7 =  new Colaborador();
        assertThat(colaboradorService.save(colaborador)).isEqualTo(colaborador7);



    }
}
