package com.mobicare.colaborador.controller;


import com.mobicare.colaborador.interfaces.BaseController;
import com.mobicare.colaborador.library.IdadeService;
import com.mobicare.colaborador.model.Colaborador;
import com.mobicare.colaborador.model.ColaboradorVo;
import com.mobicare.colaborador.model.ColaboradorDto;
import com.mobicare.colaborador.service.ColaboradorService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;

@RestController
@RequestMapping(value = "/api/colaboradores", produces = MediaType.APPLICATION_JSON_VALUE)
public class ColaboradorController extends BaseController<ColaboradorDto, Colaborador, ColaboradorService, Long> {

    @Autowired
    ColaboradorService colaboradorService;
    private ColaboradorVo mapperVo;

    @Autowired
    protected PagedResourcesAssembler<ColaboradorVo> assembler;
    @Autowired
    private IdadeService idade;

    @PostConstruct
    public void ColaboradorController(){ super.BaseController(new Colaborador(), new ColaboradorDto());}


    @GetMapping(value = "/colaboradoresSetores", produces = {"application/json","application/x-yaml"})
    public ResponseEntity<?> findAllColaboradesSetores(@RequestParam(value = "page", defaultValue = "0") int page,
                                     @RequestParam(value = "limit", defaultValue = "12") int limit) {

        Pageable pageable = PageRequest.of(page,limit);
        var pagemodels =  colaboradorService.findAllColaboradoresSetores(pageable);
        pagemodels.stream().forEach(c-> c.setIdade(idade.getIdade(c.getDtnascimento())));

        Page<ColaboradorVo> models = pagemodels.map(this::convertVo);

        PagedModel<EntityModel<ColaboradorVo>> pagedModel = assembler.toModel(models);

        return new ResponseEntity<>(pagedModel, HttpStatus.OK);
    }

    public ColaboradorVo convertVo(Object obj) {
        mapperVo = new ColaboradorVo();
        return  new ModelMapper().map(obj, mapperVo.getClass());
    }

}
