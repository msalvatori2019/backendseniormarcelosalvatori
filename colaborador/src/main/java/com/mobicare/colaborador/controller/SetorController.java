package com.mobicare.colaborador.controller;




import com.mobicare.colaborador.interfaces.BaseController;
import com.mobicare.colaborador.model.Setor;
import com.mobicare.colaborador.model.SetorDto;
import com.mobicare.colaborador.service.SetorService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;

@RestController
@RequestMapping(value = "/api/setores", produces = MediaType.APPLICATION_JSON_VALUE)
public class SetorController extends BaseController<SetorDto, Setor, SetorService, Long> {

    @PostConstruct
    public void SetorController(){ super.BaseController(new Setor(), new SetorDto());}
}
