package com.mobicare.colaborador.service;


import com.mobicare.colaborador.library.BaseService;
import com.mobicare.colaborador.library.IdadeService;
import com.mobicare.colaborador.model.BlackList;
import com.mobicare.colaborador.model.Colaborador;
import com.mobicare.colaborador.repository.ColaboradorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Date;

/** Somente é permitido até 20% de colaboradores menores que 18 anos por setor.
 Somente é permitido até 20% de colaboradores maiores que 65 anos na empresa.
 * @author Marcelo Salvatori
 * @return true
 */


@Service
public class ColaboradorService extends BaseService<Colaborador, ColaboradorRepository, Long> {

    @Autowired
    ColaboradorRepository colaboradorRepository;

    @Autowired
    IdadeService idadeService;

    private  final String UrlVBlackList
            = "https://5e74cb4b9dff120016353b04.mockapi.io/api/v1";

    @Override
    public Colaborador save(Colaborador item) {

        if  (validaIdade(item.getDtnascimento()) &&  (validaBlackList(item.getCpf()))){
            return super.save(item);
        } else
        return new Colaborador();
    }
    public boolean validaIdade(Date date){

        int idade=0;
        idade = idadeService.getIdade(date);

        long idadeMenor18    = 0;
        long idadeMaior65    = 0;
        long qtdpercent = 0;
        long qtd        = 0;

        if ((idade < 18) || (idade > 65)) {
            if (idade < 18){
                idadeMenor18 =  colaboradorRepository.findIdade18();
                idadeMenor18 = idadeMenor18 + 1;
            } else if (idade > 65) {
                idadeMaior65 =  colaboradorRepository.findIdade65();
                idadeMaior65 = idadeMaior65 + 1;
            }
            qtd =  colaboradorRepository.findQtd();
            qtdpercent  = (qtd * 20) / 100;
            if ((idadeMenor18 > qtdpercent) || (idadeMaior65 > qtdpercent))
                return false;
            else
                return true;
        }else
            return true;
    }
    public boolean validaBlackList(String cpf) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<BlackList[]> response =
                restTemplate.getForEntity(
                        UrlVBlackList.concat("/blacklist?search=").concat(cpf),
                        BlackList[].class);
        BlackList[] blackLists = response.getBody();

      return blackLists.length> 0 ? true :false;

    }

    public Page<Colaborador> findAllColaboradoresSetores(Pageable pageable) {
        return colaboradorRepository.findAllcolaboradorSetor(pageable);
    }
}
