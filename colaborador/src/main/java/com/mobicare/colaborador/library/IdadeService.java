package com.mobicare.colaborador.library;

import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;

@Service
public class IdadeService {
    int idade = 0;

    public int getIdade(Date date){

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        Date date2 = new Date();
        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(date2);
        idade = calendar2.get(Calendar.YEAR) - calendar.get(Calendar.YEAR);
        if (calendar2.get(Calendar.MONTH) < calendar.get(Calendar.MONTH))
            idade = idade -1;
        else
        if (calendar2.get(Calendar.MONTH) >=  calendar.get(Calendar.MONTH))  {
            if (calendar2.get(Calendar.DAY_OF_MONTH) <  calendar.get(Calendar.DAY_OF_MONTH))
                idade = idade -1;
        }

        return  idade;
    }
}
