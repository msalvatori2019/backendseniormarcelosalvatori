package com.mobicare.colaborador.repository;


import com.mobicare.colaborador.model.Setor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
    public interface SetorRepository extends PagingAndSortingRepository<Setor, Long> {
}
