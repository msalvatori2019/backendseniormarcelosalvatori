package com.mobicare.colaborador.model;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;

import java.util.ArrayList;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonDeserialize(as = BlackList.class)
public class BlackList {


    private Long id;

    private String createdAt;

    private String name;

    private String cpf;

}
