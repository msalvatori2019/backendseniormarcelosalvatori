package com.mobicare.colaborador.model;


import com.mobicare.colaborador.interfaces.DTOEntity;
import com.mobicare.colaborador.library.BaseBean;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="setor")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Setor extends BaseBean<Long> implements Serializable , DTOEntity {

    private static final long serialVersionUID = -6238842710331987382L;

    @Id
    private Long id;

    @Length(min = 1, max = 200, message = "Descricao deve conter entre 1 e 200  caracteres")
    @Column(name = "descricao",nullable = false)
    private String descricao;

    @Override
    public Long getIdEntity() {
        return  this.id;
    }
}
