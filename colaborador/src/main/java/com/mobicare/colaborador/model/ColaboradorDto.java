package com.mobicare.colaborador.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mobicare.colaborador.interfaces.DTOEntity;
import lombok.*;
import org.springframework.hateoas.RepresentationModel;

import java.io.Serializable;
import java.util.Date;

@JsonPropertyOrder({"nome","email","setor"})
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ColaboradorDto extends RepresentationModel<ColaboradorDto> implements Serializable, DTOEntity {

    private static final long serialVersionUID = -8859600088046283155L;

    @JsonProperty("id")
    private Long id;

    @JsonProperty("cpf")
    private String cpf;

    @JsonProperty("nome")
    private String nome;

    @JsonProperty("email")
    private String email;

    @JsonProperty("telefone")
    private String telefone;

    @JsonProperty("dtnascimento")
    private Date dtnascimento;

    @JsonProperty("setor")
    private SetorDto setor;



}
