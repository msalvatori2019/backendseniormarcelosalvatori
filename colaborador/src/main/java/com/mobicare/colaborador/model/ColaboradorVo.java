package com.mobicare.colaborador.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mobicare.colaborador.interfaces.DTOEntity;
import lombok.*;
import org.springframework.hateoas.RepresentationModel;

import java.io.Serializable;

@JsonPropertyOrder({"nome","email","setor","idade"})
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ColaboradorVo extends RepresentationModel<ColaboradorVo> implements Serializable, DTOEntity {

    private static final long serialVersionUID = -8859600088046283155L;

    @JsonProperty("nome")
    private String nome;

    @JsonProperty("email")
    private String email;

    @JsonProperty("setor")
    private SetorDto setor;

    @JsonProperty("idade")
    private int idade;

}
